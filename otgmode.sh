#!/bin/bash
# A sample Bash script, by cr1tikal

# /etc/modules template
read -r -d '' DEFAULT << EOM
# /etc/modules: kernel modules to load at boot time.
#
# This file contains the names of kernel modules that should be loaded
# at boot time, one per line. Lines beginning with "#" are ignored.

i2c-dev
dwc2
EOM

# Check for root permission
if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit
fi

update_modules () {
    echo "Applying changes..."
    rm /etc/modules
    cp /etc/modules.bak /etc/modules
    echo "$1" >> /etc/modules
    #read_file=`cat /etc/modules.bak`
    #echo "$read_file"
}

create_default_modules () {
    echo "$DEFAULT" > /etc/modules.bak
}

# Check for /etc/modules backup files
echo "Checking for backup file..."
if [ -e /etc/modules.bak ]
then
    echo Backup file exists
else
    echo Backup file not exists. Creating the backup file now...
    create_default_modules
fi

echo
echo Select module:
echo  1\) Serial
echo  2\) Ethernet
echo  3\) Mass Storage
echo  4\) MIDI
echo  5\) Audio
echo  6\) Keyboard/Mouse
echo  7\) Mass Storang and Serial
echo  8\) Ethernet and Serial
echo  9\) Multi
echo 10\) Webcam
echo 11\) Printer
echo 12\) Gadget Tester
echo
echo Selection: 

read varname
echo

case $varname in
    1)
        echo Serial mode is selected...
        update_modules g_serial
        ;;
    2)
        echo USB Ethernet mode is selected...
        update_modules g_ether
        ;;
    *)
        echo Mode not yet supported.
        ;;
esac

echo
echo "Restart the system now to apply changes? [y/n]"
read restart_system
echo

case $restart_system in
    y)
        ;&
    Y)
        echo "Restarting the system..."
        reboot
        ;;
    n)
        ;&
    N)
        ;&
    *)
        exit
        ;;
esac
    