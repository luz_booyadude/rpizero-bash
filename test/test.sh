#!/bin/bash
#
# Installer for making system data read only
#
# Usage:
# 	chmod +x readonly-system.sh
#	./readonly-system.sh
#

# value=`cat test`
# echo "${value//'/run/dhcpcd.pid'/'/var/run/dhcpcd.pid'}" | tee test

sed '/RemainAfterExit=yes/a ExecStartPre=/bin/echo "" >/tmp/random-seed' test | tee test