#!/bin/bash
#
# Installer for making system data read only
#
# Usage:
# 	chmod +x readonly-system.sh
#	./readonly-system.sh
#

RED='\033[0;31m'
GRN='\033[0;32m'
YLW='\033[1;33m'
LBL='\033[1;34m'
NC='\033[0m' # No Color

printf "${YLW}This will make the system read-only mode capable. Can switch back and forth using ro or rw command.${NC}\n"
printf "${YLW}Continue? [y/n]\n${NC}"
read confirm_setup
if [[ $confirm_setup == y* ]]
then
    printf "${YLW}\nUpdating the system...\n${NC}"
else
    exit
fi

# Update Packages and ensure dependencies are installed
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y screen git rsync gdisk insserv

# Reboot in case of bootloader or kernel version change

# Remove unwanted software
sudo apt-get remove --purge wolfram-engine triggerhappy anacron logrotate dphys-swapfile xserver-common lightdm
sudo insserv -r x11-common
sudo apt-get autoremove --purge

# Replace log management with busybox one
sudo apt-get install busybox-syslogd
sudo dpkg --purge rsyslog

# Disable swap
## Backup old cmdline.txt
sudo cp /boot/cmdline.txt /boot/cmdline.bak
sudo rm /boot/cmdline.txt
sudo cp -f ~/rpizero-bash/readonly-system/cmdline.txt /boot/

# Move some system files to temp filesystem
sudo rm -rf /var/lib/dhcp/ /var/lib/dhcpcd5 /var/run /var/spool /var/lock /etc/resolv.conf
sudo ln -s /tmp /var/lib/dhcp
sudo ln -s /tmp /var/lib/dhcpcd5
sudo ln -s /tmp /var/run
sudo ln -s /tmp /var/spool
sudo ln -s /tmp /var/lock
sudo touch /tmp/dhcpcd.resolv.conf
sudo ln -s /tmp/dhcpcd.resolv.conf /etc/resolv.conf

# Move some lock file to temp File System
dhcpcd5=`cat /etc/systemd/system/dhcpcd5`
echo "${dhcpcd5//'/run/dhcpcd.pid'/'/var/run/dhcpcd.pid'}" | sudo tee /etc/systemd/system/dhcpcd5

# Move random-seed file to writable location
sudo rm /var/lib/systemd/random-seed
sudo ln -s /tmp/random-seed /var/lib/systemd/random-seed
sed '/RemainAfterExit=yes/a ExecStartPre=/bin/echo "" >/tmp/random-seed' /lib/systemd/system/systemd-random-seed.service | sudo tee /lib/systemd/system/systemd-random-seed.service

# Reload systemd
sudo systemctl daemon-reload

# Remove some startup scripts
sudo insserv -r bootlogs
sudo insserv -r console-setup